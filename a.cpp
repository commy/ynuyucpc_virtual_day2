#include <iostream>
#include <string>
 
using namespace std;
 
int main() {
    string s;
    cin >> s;
    int n = s.size();
    for (int i = 0; i < n / 2; i++) {
	char a = s[i], b = s[n - i - 1];
	if (a != '*' && b != '*' && a != b) {
	    cout << "NO" << endl;
	    return 0;
	}
    }
    cout << "YES" << endl;
    return 0;
}