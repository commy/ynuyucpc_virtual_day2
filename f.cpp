#include <iostream>
#include <algorithm>
#include <vector>
#include <map>
 
using namespace std;
using ll = long long;
 
const ll MOD = 1000000007;
 
int main() {
    ll A, B;
    cin >> A >> B;
    map<ll, ll> m;
    for (ll n = B + 1; n <= A; n++) {
	ll N = n;
	for (ll i = 2; i * i <= n; i++) {
	    while (N % i == 0) {
		m[i]++;
		N /= i;
	    }
	}
	if (N != 1) {
	    m[N]++;
	}
    }
    ll ans = 1;
    for (auto &it :m) {
	ans = (ans * (it.second + 1)) % MOD;
    }
    cout << ans << endl;
    return 0;
}