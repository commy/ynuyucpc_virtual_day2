#include <iostream>
#include <algorithm>
#include <vector>
 
using namespace std;
using ll = long long;
 
int main() {
    int C;
    cin >> C;
    vector<int> Max(3, 0);
    for (int i = 0; i < C; i++) {
	vector<int> In(3);
	for (int j = 0; j < 3; j++) {
	    cin >> In[j];
	}
	sort(begin(In), end(In));
	for (int j = 0; j < 3; j++) {
	    Max[j] = max(Max[j], In[j]);
	}
    }
    cout << Max[0] * Max[1] * Max[2] << endl;
    return 0;
}