### a 高橋くんと回文

文字列を前からと後ろからと両方見ていったときに，その文字が

* アルファベットで同じであるか
+ 最低でも一方が$`*`$であるか

のいずれかを満たすと，ある文字について回文条件を満たします．
これをすべての文字で確認すればよいです．

### b メタ構文変数

ある2つの集合についてその積集合と，和集合を$`O(NlogN)`$で見ていくことができればよいです．

ある集合$`A,B`$について，その和集合は$`|A∪B|=|A|+|B|-|A∩B|`$のようになるので，結局積集合のみを求めることができればよいです．

積集合を$`O(NlogN)`$で求めるアルゴリズムはたくさんあるのですが，集合Aの要素がBに含まれているかを二分探索などで求めれば，全体として$`O(NlogN)`$で求めることが可能です．

### c 引越しできるかな

ある1つの荷物のタテ，ヨコ，高さは回転することで自由に変更することができます．

ある荷物の(タテ，ヨコ，高さ)の最大値を$`L_{max}`$とすると，別の荷物において，$`L_{max}`$以下の(タテ，ヨコ，高さ)は無視することができます．出来る限り容量を小さくしたいので，各荷物大きいものはまとめ，高さに割り振るようにします．

このように考えると，(タテ，ヨコ，高さ)を各荷物ごとにソートして，(タテ，ヨコ，高さ)それぞれの最大値を求めてその積を求めればよいです．

### d アットコーダー王国のコンテスト事情

最終的にかかる時間は，

* $`(T_1)+(T_1+T_2)+ \cdots + (T_1+T_2+\cdots+T_n) = n*T_1+(n-1)*T_2+\cdots+T_n`$

となります．これを小さくするには$`n`$にかかる$`T_1`$がなるべく小さいほうがいいです．同様に$`T_2`$も($`T_1`$についで)小さいほうがいいです．よって$`T_i`$を昇順にソートして上記の式を求めれば最短が求まります．

組み合わせ個数ですが，解く時間が同じ問題が$`m`$個あるとすれば，この$`m`$個は好きな順番で解いていいです．それ以外は入れ替えると時間が増加してしまいます．

よって，ある数字が$`m`$個あるとき，$`m!`$をかけると答えが求まります．

### e 最短路問題

まず先頭が0でないものは0です．また0が2つ以上あっても答えは0です．それ以外のケースを考えていきます．

まず最短経路が1の頂点はどうつながるべきでしょうか？最短経路0の頂点からつながるべきです．

では最短経路が2の頂点はどうでしょうか？同じように最短経路1の頂点からつながるべきです．

このことから，最短経路$`n+1`$の頂点は$`n`$の頂点からつながることが分かります．
最短経路$`n+1`$の頂点は$`n-1`$以下の頂点とはつながりません．つながってしまうと最短経路が更新されてしまうからです．

同じように$`n+3`$以上の頂点ともつながりません．$`n+3`$以上の頂点の最短経路を更新してしまうからです．

また最短経路$`n`$の頂点は$`n`$同士でつながっても問題ありません．この操作で最短経路は変化しません．

以上の考察から頂点$`n`$を考える時に，$`n-1`$の頂点が$`m`$個あった場合，$`n-1`$の頂点のどの頂点とつながっても問題ありません．複数繋がっても問題ありません．ただし，つながらないのは問題があります．よって$`2^m - 1`$個のつながり方があります．これが$`n`$の頂点数分($`t`$)考える必要があります($`(2^m-1)^t`$)．

また$`n`$頂点同士は自由につながってよいので$`_tC_2`$個の辺を付けるかつけないかを選択可能です．よって$`2^{(_tC_2)}`$を計算します．

$`x^n`$の計算には繰り返し二乗法を用いて高速化することで解くことができます．

### f 約数かつ倍数

ある数$`X`$の約数はどういう風に考えることができるでしょうか？$`X`$を素因数分解して$`X=p^{x_1}*q^{x_2}*r^{x_3}*\cdots`$と分解できたとします．この時$`X`$の約数は$`p`$を0から$`x_1`$個，$`q`$を0から$`x_2`$個...のように選んだ数になります．

同じように倍数はどうなるでしょうか？$`p`$を$`x_1`$個以上，$`q`$を$`x_2`$個以上...のように最低限選ぶべき個数は決まります．またここに無い素因数$`s`$を使用することもできます．

このことを頭に入れて考えると，$`A!`$の約数であって$`B!`$の倍数というのは，$`A!`$を素因数分解した結果$`p^{a_1}*q^{a_2}\cdots`$と$`B!`$を素因数分解した結果$`p^{b_1}*q^{b_2}\cdots`$から，$`p`$を$`b_1`$から$`a_1`$個，$`q`$を$`b_2`$から$`a_2`$個...のように選ぶことになります．

それぞれ階乗による結果なので，結局$`b_1`$から$`a_1`$というのは，$`B+1`$から$`A`$までの数について，素因数$`p`$の個数を数えればいいです．$`A-B \le 100, A,B \le 10^9`$なので，素因数分解を$`O(\sqrt{A})`$で行うことで解くことができます．

### g A mod B Problem

まず1つのブロック($`A_iがL_i個`$)について考えます．$`A_i`$の桁数が$`d_i`$とすると，この数は$`A_i+A_i*10^{d_i}+\cdots+A_i*10^{d_i*(L_i-1)}`$と書きあらわすことができます．

このまとまりを$`A^s_i`$，桁数を$`d^s_i`$とすると，最終的な答えは$`A^s_1*10^{d^s_2+\cdots d^s_n} + A^s_2*10^{d^s_3+\cdots d^s_n} + \cdots + A^s_n`$を$`B`$で割った余りになります．繰り返し二乗法で$`10^x`$は高速に計算できるため，あとは$`A^s_i`$ mod $`B`$を計算できればいいです．

$`A^s_i=A_i+A_i*10^{d_i}+\cdots+A_i*10^{d_i*(L_i-1)}=A_i*(1+10^{d_i}+\cdots+10^{d_i*(L_i-1)})`$です．

分かりやすいように$`10^{d_i}=x`$と置き換えると，$`A^s_i=A_i*(1+x+x^{2}+\cdots+x^{L_i-1})`$です．

この式を等比数列の和の公式を使用して$`x-1`$の(mod $`B`$での)逆元を求めると，99点獲得できます．

100点は次の事実を使用します．

$`S(x, n)=1+x+\cdots+x^{n-1}`$とすると，$`S(x, n) = 1 + x*S(x, n - 1)`$．特に$`n`$が偶数であれば$`S(x, n) = S(x^2, \frac{n}{2}) + x * S(x ^ 2, \frac{n}{2})`$．

この方法は繰り返し二乗法と同様に$`S(x, n)`$を$`O(logN)`$で求めることができます．また計算途中に除算が発生しないため，$`B`$が素数でなくても計算可能です．

### h タコヤ木

まず-1の区間は独立して考えることができます．ですので，ある区間について考えます．-1の連続する長さを$`n`$，その差を$`d`$とします．この区間は，$`n`$日でぴったり$`d`$だけ増えればいいです．もう少し正確にいうと，$`n+1`$回の増加で$`d`$を作ることができればよいです．
(例えば$`n=2`$で$`d=4`$の時(10, -1(a), -1(b), 14)，変化量だけ考えると，10->a，a->b, b->14の3回で合計$`d`$増える必要がある．)

これの方法として組み合わせを利用します．変化しなければいけない値は$`d`$で，その間に$`n`$個の区切りを入れます．こんなイメージです(○|○○|○)．

よって組み合わせとして，$`_{n+d}C_n`$を計算します．$`n \le 2000, d \le 10^9`$なので，少し工夫が必要です．$`_{n+d}C_n=\frac{(n+d)!}{d!n!}=\frac{_{n+d}P_{n}}{n!}`$ですから，毎回$`n`$回ループを回すことで計算可能です．

よって全体で計算量は間に合います．



