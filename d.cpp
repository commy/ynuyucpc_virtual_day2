#include <iostream>
#include <vector>
#include <algorithm>
 
using namespace std;
using ll = long long;
 
const ll mod = 1000000007;
 
int main() {
    int N;
    cin >> N;
    vector<ll> T(N);
    for (auto &x: T) cin >> x;
    sort(begin(T), end(T));
    ll ans = 0;
    int cnt = 1;
    int prev = -1;
    ll res = 1;
    for (int i = 0; i < N; i++) {
	ans += T[i] * (N - i);
	if (prev == T[i]) {
	    cnt++;
	}
	else {
	    cnt = 1;
	}
	prev = T[i];
	res = (res * cnt) % mod;
    }
    cout << ans << endl;
    cout << res << endl;
    
    return 0;
}