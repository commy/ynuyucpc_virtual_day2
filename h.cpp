#include <iostream>
#include <vector>
 
#define REP(i,a,b) for(int i=int(a);i<int(b);i++)
 
using ll = long long;
using namespace std;
 
const ll mod = 1000000007;
 
ll fast_pow(ll x, ll n) {
    if (n == 0) return 1;
    else if (n % 2) {
	return (x * fast_pow(x, n - 1)) % mod;
    } else {
	ll t = fast_pow(x, n / 2);
	return (t * t) % mod;
    }
}
 
int main() {
    int N;
    cin >> N;
    vector<ll> A(N);
    REP(i,0,N) {
	cin >> A[i];
    }
 
    vector<ll> fac(N + 1, 1);
    vector<ll> revfac(N + 1, 1);
    REP (i,0,N) {
	fac[i + 1] = (fac[i] * (i + 1)) % mod;
    }
    revfac[N] = fast_pow(fac[N], mod - 2);
    for (int i = N - 1; i > 0; i--) {
	revfac[i] = (revfac[i + 1] * (i + 1)) % mod;
    }
 
    auto nHr = [&](ll n, ll r) {
	ll res = revfac[n - 1];
	for (ll s = r + 1; s <= r + n - 1; s++) {
	    res = (res * s) % mod;
	}
	return res;
    };
    
    ll ans = 1;
    int lb = 0;
    while(lb < N) {
	if (A[lb] != -1) {
	    int ub = lb + 1;
	    while (ub < N && A[ub] == -1) {
		ub++;
	    }
	    ans = (ans * nHr(ub - lb, A[ub] - A[lb])) % mod;
	    lb = ub;
	} else {
	    lb++;
	}
    }
    cout << ans << endl;
    return 0;
}