#include <iostream>
#include <algorithm>
#include <vector>
#include <iomanip>
 
using namespace std;
using ll = long long;
 
int main() {
    int N, M;
    cin >> N >> M;
    vector<int> A(N), B(M);
    for (auto &a : A) cin >> a;
    for (auto &b : B) cin >> b;
    sort(begin(A), end(A));
    int s = 0;
    for (auto b : B) {
	s += binary_search(begin(A), end(A), b);
    }
    cout << fixed << setprecision(10) << double(s) / (N + M - s) << endl;
    return 0;
}