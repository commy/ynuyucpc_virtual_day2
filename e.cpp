#include <iostream>
#include <algorithm>
#include <vector>
#include <iomanip>
 
using namespace std;
using ll = long long;
 
const ll MOD = 1000000007;
 
ll fast_pw(ll x, ll n) {
    if (n == 0) {
	return 1LL;
    }
    else if (n % 2) {
	return (x * fast_pw(x, n - 1)) % MOD;
    }
    else {
	ll res = fast_pw(x, n / 2) % MOD;
	return (res * res) % MOD;
    }
}
 
 
int main() {
    int N;
    cin >> N;
    vector<ll> A(N + 1, 0);
    for (int i = 0; i < N; i++) {
	int a;
	cin >> a;
	A[a]++;
	if (i == 0 && a != 0) {
	    cout << 0 << endl;
	    return 0;
	}
    }
    
    if (A[0] != 1) {
	cout << 0 << endl;
	return 0;
    }
    while (N > 0 && A[N - 1] == 0) {
	N--;
    }
    ll ans = 1;
    for (int i = 1; i < N; i++) {
	ans = (ans * fast_pw(2, A[i] * (A[i] - 1) / 2)) % MOD;
	ll t = fast_pw(fast_pw(2, A[i - 1]) - 1, A[i]) % MOD;
	ans = (ans * t) % MOD;
    }
    cout << ans << endl;
    return 0;
}