#include <iostream>
#include <algorithm>
#include <vector>
 
using namespace std;
using ll = long long;
 
ll fast_pow(ll x, ll n, ll MOD) {
    if (n == 0) {
	return 1LL % MOD;
    }
    else if (n % 2) {
	return (x * fast_pow(x, n - 1, MOD)) % MOD;
    }
    else {
	ll res = fast_pow(x, n / 2, MOD);
	return (res * res) % MOD;
    }
}
 
int digit(ll n) {
    int ans = 0;
    do {
	n /= 10;
	ans++;
    } while (n > 0);
    return ans;
}
 
ll fast_sum(ll bs, ll n, ll MOD) {
    if (n == 0) {
	return 0LL;
    }
    else if (n % 2) {
	return (1 + (bs * fast_sum(bs, n - 1, MOD)) % MOD) % MOD;
    } else {
	ll res = fast_sum((bs * bs) % MOD, n / 2, MOD);
	return (res + ((res * bs) % MOD)) % MOD;
    }
}
 
 
int main() {
    int N;
    cin >> N;
    vector<ll> a(N), L(N);
    for (int i = 0; i < N; i++) {
	cin >> a[i] >> L[i];
    }
    ll B;
    cin >> B;
    ll ans = 0;
    ll len = 0;
    for (int i = N - 1; i >= 0; i--) {
	int d = digit(a[i]);
	ll res = fast_sum(fast_pow(10, d, B), L[i], B);
	res = (res * a[i]) % B;
	ans = (ans + (fast_pow(10, len, B) * res) % B) % B;
	len += d * L[i];
    }
    cout << ans << endl;
    return 0;
}